# Control de Versiones 

![image](img/goku.jpg)

## Indice

1. [Introducción](introduccion.md)
2. [Contenidos](contenido.md)
3. [Git](git.md)

## Referencias
 - [Seidor](https://www.drauta.com/5-softwares-de-control-de-versiones)

-  [Wikipedia](https://es.wikipedia.org/wiki/Wikipedia:Portada)
## Autores

- [Jairo Verdugo Mesa](https://gitlab.com/Jairoverdugo98)
- [Larry Reynoso García](https://gitlab.com/LarryWestbrook)

## Licencias

![image](img/licencia.png)
